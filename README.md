# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Computer & human compete to predict next letter of words
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Navigate to the **"Downloads"** link on the lower left section
* Download the repository
* Extract the files to a folder
* Navigate to the **Lib** folder
* Extract the **console_app** zip file
* Double click **WordPlay** to run the app

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact