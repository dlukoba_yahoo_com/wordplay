﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using WordPlay;

namespace WordPlayTest
{
    [TestClass]
    public class WordPlayTests
    {
        WordPlayService _wordplayService;
        Mock<WordPlayService> _mockWordplayService;

        [TestInitialize]
        public void SetUp()
        {
            var wordPlayServiceMock = new Mock<WordPlayService>();
            _mockWordplayService = wordPlayServiceMock;
            _wordplayService = wordPlayServiceMock.Object;
        }

        [TestMethod]
        public void If_PlayerPlaysFirstLetterThatDoesNotStartAWord_Then_ThePlayerLoses()
        {
            var wordsList = new List<WordListItemModel>();
            
            var playState = _wordplayService.Play(Player.Computer, Player.Computer, "X", ref wordsList);

            Assert.AreEqual(Player.Computer, playState.LosingPlayer);
            Assert.IsTrue(playState.IsGameOver);
        }

        [TestMethod]
        public void If_PlayerPlaysLastLetterInAWord_Then_ThePlayerLoses()
        {
            var wordsList = new List<WordListItemModel>()
                {
                    new WordListItemModel{ Word = "caterer" }
                };

            var playState = _wordplayService.Play(Player.Computer, Player.Computer, "caterer", ref wordsList);

            Assert.AreEqual(Player.Computer, playState.LosingPlayer);
            Assert.IsTrue(playState.IsGameOver);
        }

        [TestMethod]
        public void If_PlayerPlaysALetterThatDoesNotMakeUpAWord_Then_ThePlayerLoses()
        {
            var wordsList = new List<WordListItemModel>()
            {
                new WordListItemModel { Word = "catering" }
            };
            
            var playState = _wordplayService.Play(Player.Computer, Player.Computer, "caterea", ref wordsList);

            Assert.AreEqual(Player.Computer, playState.LosingPlayer);
            Assert.IsTrue(playState.IsGameOver);
        }

        [TestMethod]
        public void If_PlayerPlaysALetterThatThatIsPartOfAWord_Then_TheGameIsNotOver()
        {
            var wordsList = new List<WordListItemModel>()
            {
                new WordListItemModel { Word = "caterers" }
            };
            
            var playState = _wordplayService.Play(Player.Computer, Player.Computer, "caterer", ref wordsList);

            Assert.IsFalse(playState.IsGameOver);
            Assert.IsNull(playState.LosingPlayer);
        }

        [TestMethod]
        public void If_PlayerSelectsFirstLetter_Then_ComputerShouldPreferOddWords_IfAvailableInList()
        {
            var wordsList = new List<WordListItemModel>()
            {
                new WordListItemModel{ Word = "oda" },
                new WordListItemModel{ Word = "odre" }
            };
            
            string nextLetter = _wordplayService.PredictNextLetter(Player.Computer, Player.User, "od", ref wordsList);

            Assert.AreEqual("a", nextLetter);
        }

        [TestMethod]
        public void If_ComputerSelectsFirstLetter_Then_ComputerShouldPreferEvenWords_IfAvailableInList()
        {
            var wordsList = new List<WordListItemModel>()
            {
                new WordListItemModel{ Word = "oda" },
                new WordListItemModel{ Word = "odre" }
            };
            
            string nextLetter = _wordplayService.PredictNextLetter(Player.Computer, Player.Computer, "od", ref wordsList);

            Assert.AreEqual("r", nextLetter);
        }
    }
}
