﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPlay
{
    public class WordListItemModel
    {
        public string Word { get; set; }
        public int Length { get { return Word.Length; } }
    }
}
