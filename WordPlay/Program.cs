﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPlay
{
    class Program
    {
        static void Main(string[] args)
        {            
            Console.WriteLine("***************WELCOME TO WORD PLAY******************");

            InitiateNewGame();

            Console.ReadKey();
        }

        static void InitiateNewGame()
        {
            Console.WriteLine("Press key 'n' to play a new game");
            ConsoleKeyInfo playNewGame = Console.ReadKey();

            if (playNewGame.KeyChar.ToString().Equals("n", StringComparison.OrdinalIgnoreCase))
                PlayNewGame();
        }

        static void PlayNewGame()
        {
            WordPlayService wordplayService = new WordPlayService();
            bool isGameOver = false;
            Console.WriteLine("Would you like to play first? [y/n]");
            int firstPlayerAns = Console.Read();
            string firstPlayerAnsAsString = Convert.ToChar(firstPlayerAns).ToString();
            Player firstPlayer = Player.User; //by default, the user plays first

            if (firstPlayerAnsAsString.Equals("n", StringComparison.OrdinalIgnoreCase))
            {
                firstPlayer = Player.Computer;
            }
            Player currentPlayer = firstPlayer;
            string currentWord = string.Empty;
            List<WordListItemModel> wordsMatchingCurrentWord = null;

            while (!isGameOver)
            {
                string nextLetter = wordplayService.PredictNextLetter(currentPlayer, firstPlayer, currentWord,
                    ref wordsMatchingCurrentWord);
                currentWord += nextLetter;
                Console.WriteLine();
                Console.WriteLine("Current word {0}", currentWord);
                var playState = wordplayService.Play(currentPlayer, firstPlayer, currentWord, ref wordsMatchingCurrentWord);

                //if we are still in play, switch players
                if (!playState.IsGameOver)
                {
                    if (currentPlayer == Player.Computer)
                        currentPlayer = Player.User;
                    else
                        currentPlayer = Player.Computer;
                }
                else
                {
                    isGameOver = playState.IsGameOver;

                    Console.WriteLine();
                    Console.WriteLine("===========GAME OVER===============");
                    Console.WriteLine("Losing player");
                    Console.WriteLine("-------------");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(playState.LosingPlayer);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("--------------");
                    Console.WriteLine("Losing word");
                    Console.WriteLine("--------------");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(currentWord);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }

            InitiateNewGame();
        }
    }

    public enum Player : int
    {
        User = 1,
        Computer = 2
    }
}
