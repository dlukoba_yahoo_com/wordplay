﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace WordPlay
{
    public class WordPlayService
    {
        Random randomNumberGenerator;

        public WordPlayService()
        {
            randomNumberGenerator = new Random();
        }

        public PlayState Play(Player currentPlayer, Player firstPlayer, string currentWord, 
            ref List<WordListItemModel> wordsMatchingCurrentWord)
        {
            if (wordsMatchingCurrentWord == null)
            {
                wordsMatchingCurrentWord = GetMatchingWordsFromFile(currentPlayer,
                    (firstPlayer == Player.Computer) ? true : false, currentWord);
            }
            var matchedWords = wordsMatchingCurrentWord.Where(x => x.Word.StartsWith(currentWord))
                .ToList();
            wordsMatchingCurrentWord.Clear();
            wordsMatchingCurrentWord = new List<WordListItemModel>(matchedWords);
            
            //if currentword does not match any starting word in the list
            if (wordsMatchingCurrentWord.Count == 0)
            {
                return new PlayState
                {
                    CurrentWord = currentWord,
                    IsGameOver = true,
                    LosingPlayer = currentPlayer
                };
            }
            else
            {
                //check an exact match
                bool anyExactMatch = wordsMatchingCurrentWord.Any(x => x.Word == currentWord);
                if (anyExactMatch)
                {
                    return new PlayState
                    {
                        CurrentWord = currentWord,
                        IsGameOver = true,
                        LosingPlayer = currentPlayer
                    };
                }
                else
                {
                    return new PlayState
                    {
                        CurrentWord = currentWord,
                        IsGameOver = false,
                        LosingPlayer = null
                    };
                }
            }
        }

        public string PredictNextLetter(Player currentPlayer, Player firstPlayer, string currentWord, 
            ref List<WordListItemModel> wordsMatchingCurrentWord)
        {
            string letterPicked = string.Empty;

            if (currentPlayer == Player.User)
            {
                Console.WriteLine("Please select your next letter");
                //prompt user for next letter
                ConsoleKeyInfo nextLetter = Console.ReadKey();
                letterPicked = nextLetter.KeyChar.ToString().ToLower();
            }
            else if (currentPlayer == Player.Computer)
            {
                if (wordsMatchingCurrentWord == null)
                {
                    int num = randomNumberGenerator.Next(0, 26);
                    char letter = (char)('a' + num);

                    wordsMatchingCurrentWord = GetMatchingWordsFromFile(currentPlayer, 
                        (firstPlayer == Player.Computer) ? true : false, letter.ToString());
                }

                //if first player == computer, 
                //system prefer even words, else prefer odd words
                if (firstPlayer == Player.Computer)
                {
                    var evenLetteredWords = wordsMatchingCurrentWord.Where(x => x.Length % 2 == 0)
                        .ToList();
                    if (evenLetteredWords.Count > 0)
                    {
                        letterPicked = PickRandomWordAndSelectNextLetter(currentPlayer, currentWord, evenLetteredWords);
                    }
                    else
                    {
                        var oddLetteredWords = wordsMatchingCurrentWord.Where(x => x.Length % 2 != 0)
                        .ToList();
                        letterPicked = PickRandomWordAndSelectNextLetter(currentPlayer, currentWord, oddLetteredWords);
                    }
                }
                else
                {
                    var oddLetteredWords = wordsMatchingCurrentWord.Where(x => x.Length % 2 != 0)
                        .ToList();
                    if (oddLetteredWords.Count > 0)
                    {
                        letterPicked = PickRandomWordAndSelectNextLetter(currentPlayer, currentWord, oddLetteredWords);
                    }
                    else
                    {
                        var evenLetteredWords = wordsMatchingCurrentWord.Where(x => x.Length % 2 == 0)
                        .ToList();
                        letterPicked = PickRandomWordAndSelectNextLetter(currentPlayer, currentWord, evenLetteredWords);
                    }
                }
                
                Console.WriteLine("Computer predicted letter {0}", letterPicked);
            }
            return letterPicked;
        }

        string PickRandomWordAndSelectNextLetter(Player currentPlayer, string currentWord, List<WordListItemModel> words)
        {
            if (currentPlayer != Player.Computer)
                throw new InvalidOperationException("This method can only be invoked by the comp player");

            int randomIndex = randomNumberGenerator.Next(1, words.Count + 1);
            string wordPicked = words.ElementAt(randomIndex - 1)
                .Word;
            string letterPicked = wordPicked.Substring(currentWord.Length, 1);
            return letterPicked;
        }

        public List<WordListItemModel> GetMatchingWordsFromFile(Player currentPlayer, 
            bool preferWordsWithEvenCount, string searchTerm)
        {
            string wordsFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "words.txt");
            List<WordListItemModel> matchingWords = new List<WordListItemModel>();

            using (var reader = new System.IO.StreamReader(wordsFile))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    
                    if (!string.IsNullOrEmpty(line) && 
                        line.StartsWith(searchTerm) && 
                        line.Length != searchTerm.Length)
                    {
                        matchingWords.Add(new WordListItemModel { Word = line });
                    }
                }
            }
            return matchingWords;
        }
    }
}
