﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPlay
{
    public class PlayState
    {
        public bool IsGameOver { get; set; }
        public Player? LosingPlayer { get; set; }
        public string CurrentWord { get; set; }
    }
}
